# KV Storage Server

[![pipeline status](https://gitlab.com/kv-storage/backend/badges/master/pipeline.svg)](https://gitlab.com/kv-storage/backend/commits/master)
[![coverage report](https://gitlab.com/kv-storage/backend/badges/master/coverage.svg)](https://gitlab.com/kv-storage/backend/commits/master)

Simple Web UI for KV Storage. Built with Express.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:5000/`.

## Build

Run `npm run tsc` to build the project. The build artifacts will be stored in the `build/` directory.

## Deploy

To deploy the application to the stage server it is required to commit the changes to the GitLab,
after CI will verify the code does not break the tests, the Test Runner will automatically compile
and bundle the application and push it to the stage server, where it would be placed inside of a docker container.

## Running integration tests

Run `npm run e2e` to execute the end-to-end tests via Chai and Mocha.
