import { app } from './';
import * as chai from 'chai';
import * as request from 'supertest';

declare var describe, it;

const expect = chai.expect;
const mockPosition = { position: 'testVal' };
const mockPositionModified = { position: 22222 };

describe('Create New Entry', function() {
  it('should create and return new entry', function(done) {
    request(app)
      .post('/createKey')
      .send(mockPosition)
      .end(function(err, res: any) {
        expect(res.body.position).to.be.equal(mockPosition.position);
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });

  it('should update an entry', function(done) {
    request(app)
      .put('/updateKey')
      .send(mockPositionModified)
      .end(function(err, res: any) {
        expect(res.body.position).to.be.equal(mockPositionModified.position);
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });

  it('should read an entry', function(done) {
    request(app)
      .get('/readKey/position')
      .end(function(err, res: any) {
        expect(res.body).to.be.equal(mockPositionModified.position);
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });

  it('should get all KV', function(done) {
    request(app)
      .get('/listStorage')
      .end(function(err, res: any) {
        expect(res.body).to.deep.equal(mockPositionModified);
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });

  it('should delete an entry', function(done) {
    request(app)
      .delete('/deleteKey/position')
      .end(function(err, res: any) {
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });

  it('should warn about deleting non-existing entry', function(done) {
    request(app)
      .delete('/deleteKey/nonExistingKey')
      .end(function(err, res: any) {
        expect(res.statusCode).to.be.equal(406);
        done();
      });
  });

  it('should warn about reading non-existing entry key', function(done) {
    request(app)
      .delete('/readKey/nonExistingKey')
      .end(function(err, res: any) {
        expect(res.statusCode).to.be.equal(404);
        done();
      });
  });

  it('should warn about updating non-existing entry', function(done) {
    request(app)
      .delete('/updateKey/nonExistingKey')
      .end(function(err, res: any) {
        expect(res.statusCode).to.be.equal(404);
        done();
      });
  });

  it('should warn about creating already existing entry', function(done) {
    request(app)
      .delete('/updateKey/nonExistingKey')
      .end(function(err, res: any) {
        expect(res.statusCode).to.be.equal(404);
        done();
      });
  });
});
