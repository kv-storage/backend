import * as express from 'express';
import * as bp from 'body-parser';
import * as cors from 'cors';
import { keyCheckAndSave, keySave } from './helpers';

export const app = express();
let GLOBAL_STORAGE = {};

app.use(bp.json());
app.use(cors());

// Create entry in the Storage
app.post('/createKey', (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  GLOBAL_STORAGE = keyCheckAndSave(GLOBAL_STORAGE, 'create', req.body, res, 406, 400);
});

// Update entry in the Storage
app.put('/updateKey', (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  GLOBAL_STORAGE = keyCheckAndSave(GLOBAL_STORAGE, 'update', req.body, res, 404, 400);
});

// Get entry value by key
app.get(`/readKey/:key`, (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const key = req.params.key;

  GLOBAL_STORAGE = keySave(GLOBAL_STORAGE, 'read', key, res, 404);
});

// Delete entry value by key
app.delete(`/deleteKey/:key`, (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const key = req.params.key;

  GLOBAL_STORAGE = keySave(GLOBAL_STORAGE, 'delete', key, res, 406);
});

// Show all entries in the Storage
app.get('/listStorage', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(GLOBAL_STORAGE));
});

app.listen(5000, () => {
  console.log('Server started on port 5000');
});
