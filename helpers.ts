// Get/Update the entry,
// Check if it is already in the Storage or not (depending on `type`)
// If error reject with statusA|statusB error
export const keyCheckAndSave = (GLOBAL_STORAGE, type, body, res, statusA, statusB) => {
  const key = Object.keys(body)[0];
  const ckecker = type === 'create' ? GLOBAL_STORAGE[key] === undefined : GLOBAL_STORAGE[key] !== undefined;
  if (key) {
    // Check if it is already in||not in the Storage
    if (ckecker) {
      GLOBAL_STORAGE[key] = body[key];
      res.send(JSON.stringify(body));
    } else {
      // Opposite: statusA - Now Acceptable
      res.status(statusA);
      res.send();
    }
  } else {
    // Bad JSON in the body, statusB - Bad Request
    res.status(statusB);
    res.send();
  }
  return GLOBAL_STORAGE;
};

// Show/Delete selected entry 
export const keySave = (GLOBAL_STORAGE, type, key, res, statusA) => {
  // We should proceed only if the key exists in the Storage
  if (GLOBAL_STORAGE[key] !== undefined) {
    if (type === 'delete') {
      delete GLOBAL_STORAGE[key];
      res.status(200);
      res.send();
    } else if (type === 'read') {
      res.send(JSON.stringify(GLOBAL_STORAGE[key]));
    }
  } else {
    res.status(statusA);
    res.send();
  }

  return GLOBAL_STORAGE;
};
